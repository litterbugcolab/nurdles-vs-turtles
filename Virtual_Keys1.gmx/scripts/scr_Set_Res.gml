//DEBUG
//room_goto(rm_Menu);

var base_w = 640;
var base_h = 480;
var norm_w = 640;
var norm_h = 480;
var max_w = 640;
var max_h = 480;

if os_type == os_windows || os_type == os_macosx || os_type == os_linux && os_browser == browser_not_a_browser
    {
    var aspect = window_get_height() / window_get_width();
    if aspect < norm_w / norm_h
        {
        VIEW_HEIGHT = norm_h;
        VIEW_WIDTH = norm_h / aspect;
        }
    else
        {
        VIEW_HEIGHT = norm_w * aspect;
        VIEW_WIDTH = norm_w;
        }
    display_set_gui_size(norm_w, norm_w * aspect);
    window_set_size(max_w, max_h);
    }
else
    {
    if display_get_width() > display_get_height()
        {
        var aspect = display_get_height() / display_get_width();
        }
    else
        {
        var aspect = display_get_width() / display_get_height();
        }
    if aspect < base_h / base_w
        {
        VIEW_HEIGHT = base_h;
        VIEW_WIDTH = base_h / aspect;
        }
    else
        {
        VIEW_HEIGHT = base_w * aspect;
        VIEW_WIDTH = base_w;
        }
    max_w = VIEW_WIDTH;
    max_h = VIEW_HEIGHT;
    display_set_gui_size(max_w, max_h);
    }

if os_browser == browser_not_a_browser
{
scr_Room_Set_Res(norm_w, norm_h, VIEW_WIDTH, VIEW_HEIGHT, max_w, max_h);
}
else
{
scr_Room_Set_Res(max_w, max_h, max_w, max_h, max_w, max_h);
display_set_gui_size(max_w, max_h);
}
